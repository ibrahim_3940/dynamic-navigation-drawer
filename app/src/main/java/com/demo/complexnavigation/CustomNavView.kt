package com.demo.complexnavigation

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import com.google.android.material.navigation.NavigationView

class CustomNavView : NavigationView {

    private lateinit var view: View

    constructor(context: Context) : super(context) {}
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
    }
    // Consumes touch in the NavigationView so it doesn't propagate to views below
    override fun onTouchEvent(me: MotionEvent): Boolean {
        return true
    }

    fun createHeader(res: Int) : View {
        val inflater = LayoutInflater.from(context)
        view = inflater.inflate(res, this, false)

        view.setOnTouchListener { _, event -> true }
        addView(view)
        return view
    }

    fun getHeader():View{
        return view
    }

    fun sizeMenu(view: View,headerHeight:Int) {

        val displayMetrics = resources.displayMetrics
        val screenHeight = displayMetrics.heightPixels.toFloat()

        val menuHeight = (screenHeight - headerHeight).toInt()

        val params = LayoutParams(
            LayoutParams.MATCH_PARENT,
            LayoutParams.WRAP_CONTENT
        )
        params.gravity = Gravity.BOTTOM
        params.height = menuHeight
        view.layoutParams = params
    }
}