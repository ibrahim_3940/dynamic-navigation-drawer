package com.demo.complexnavigation


import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import jp.wasabeef.blurry.Blurry
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {


    private lateinit var actionBarToggle: ActionBarDrawerToggle
    var isLoggedIN = true;

    lateinit var rootview: ViewGroup
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rootview =
            findViewById<View>(R.id.main_content) as ViewGroup
        actionBarToggle = object : ActionBarDrawerToggle(this, drawerLayout, 0, 0) {
            override fun onDrawerClosed(view: View) {
                super.onDrawerClosed(view)
                Blurry.delete(rootview)
                window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)

            }

            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)

                Blurry.with(this@MainActivity).radius(25).sampling(2).onto(rootview)
                window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)

            }
        }
        drawerLayout.addDrawerListener(actionBarToggle)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        actionBarToggle.syncState()




        navView.setNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.menu_appointments -> {
                    Toast.makeText(this, "My Profile", Toast.LENGTH_SHORT).show()
                    true
                }
                R.id.menu_artisan -> {
                    Toast.makeText(this, "People", Toast.LENGTH_SHORT).show()
                    true
                }
                R.id.menu_promoted_prop -> {
                    Toast.makeText(this, "Settings", Toast.LENGTH_SHORT).show()
                    true
                }
                else -> {
                    false
                }
            }
        }
        navView.createHeader(R.layout.nav_header_main)
        if (isLoggedIN)
            handleVisibilityWithLogin()
        else
            handleVisibilityWiohouLOgin()
    }


    override fun onSupportNavigateUp(): Boolean {
        if (this.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            this.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            drawerLayout.openDrawer(navView)
        }
        return true
    }


    override fun onBackPressed() {
        if (this.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            this.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    fun handleVisibilityWithLogin() {

        navView.getMenu().getItem(5).setActionView(R.layout.lyt_coming_soon_image);
        rl_footer_with_login.visibility = VISIBLE
        rl_footer_without_login.visibility = GONE


//        val mainLayout = findViewById<View>(R.id.drawerLayout) as DrawerLayout
//
//        val inflater = layoutInflater
//        val myLayout: View = inflater.inflate(R.layout.nav_header_main, mainLayout, false)
//
//
//        val textView = myLayout.findViewById<View>(R.id.rl_header_wihout_login) as RelativeLayout
//        textView.setBackgroundColor(resources.getColor(R.color.black))

//        val v: View = (navView as ViewGroup).getChildAt(2);
//        val tv: TextView = v.findViewById(R.id.tv_main_text)
        drawerLayout.setScrimColor(Color.parseColor("#80FFFFFF"))


        val headerView: View = navView.inflateHeaderView(R.layout.nav_header_main)
        var tv = headerView.findViewById<View>(R.id.tv_main_text) as TextView
        tv.setBackgroundColor(resources.getColor(R.color.black))
        // Log.e("index", "${navView.indexOfChild(v)}")
    }

    fun handleVisibilityWiohouLOgin() {
        rl_footer_with_login.visibility = GONE

        rl_footer_without_login.visibility = VISIBLE

    }


}